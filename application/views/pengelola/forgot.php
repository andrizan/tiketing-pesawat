<div class="panel box-shadow-none content-header">
	<div class="panel-body capitalize">
		<div class="col-md-12">
			<h3 class="animated fadeInLeft"><?= $judul ?></h3>
			<p class="animated fadeInDown capitalize">
				<?= $folder?> <span class="fa-angle-right fa"></span> <?= $p ?> <span class="fa-angle-right fa"></span>
				<?= $row->nama_depan ?> <?= $row->nama_belakang ?>
			</p>
		</div>
	</div>
</div>

<div class="form-element">
	<div class="col-md-12 padding-0">
		<div class="col-md-12">
			<div class="panel form-element-padding">
				<div class="panel-body">
					<div class="col-md-12">
						<?= form_open_multipart($url); ?>
						<input type="hidden" name="id_user" value="<?php if (isset($id)) { echo $id;} ?>">
						<div>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right">Password Baru</label>
								<div class="col-sm-10">
									<input type="password" name="password" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right">Ketik Ulang Password</label>
								<div class="col-sm-10">
									<input type="password" name="retype" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<input type="submit" value="Simpan" class="btn btn-raised btn-primary">
								<input type="reset" value="Reset" class="btn btn-raised btn-warning">
							</div>
						</div>
						<?= form_close();?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
